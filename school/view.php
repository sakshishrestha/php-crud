<style>
    table, th,td {
    background-color: #ffffff;
    border: 1px solid gray;
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    

  }
 </style>
<?php


$sql = "SELECT studentID,name, address, age,classID,teacherID,departmentID,courseID FROM student";
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);

if ($result->num_rows > 0) {
    echo "<table>
    <tr>
    
    <th>Name</th>
    <th>StudentID</th>
    <th>Address</th>
    <th>Age</th>
    <th>ClassID</th>
    <th>TeacherID</th>
    <th>DepartmentID</th>
    <th>CourseID</th>
    <th>Action</th>
    </tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr>
        
        <td>" . $row["name"]. "</td>
        <td>" . $row["studentID"]. "</td>
        <td>" . $row["address"]. "</td>
        <td>" . $row["age"]. "</td>
        <td>" . $row["classID"]. "</td>
        <td>" . $row["teacherID"]. "</td>
        <td>" . $row["departmentID"]. "</td>
        <td>" . $row["courseID"]. "</td>
        <td>
        <a onclick=\"myFunction('http://localhost/school/studentDelete.php?id=".$row["studentID"]."')\"><span class='glyphicon glyphicon-trash'></span></a>      
        <a href='http://localhost/school/studentEdit.php?id=".$row["studentID"]."'><span class='glyphicon glyphicon-pencil'></span></a></td>
        </tr>";

        echo "<script type=\"text/javascript\">
        function myFunction(newLocation) {
            if (confirm(\"Are you sure to Delete?\")) {
                console.log(newLocation);
                window.location = newLocation;
            } else {

            }
        }
        </script>";
    }
    echo "</table>";
} 
else {
    echo "0 results";
}

$conn->close();
?> 