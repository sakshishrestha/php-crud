<style>
    table, th,td {
    background-color: #ffffff;
    border: 1px solid gray;
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    

  }
 </style>
<?php

$sql = "SELECT section,classID,studentID FROM class";
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);

if ($result->num_rows > 0) {
    echo "<table>
    <tr>
    
    <th>Section</th>
    <th>StudentID</th>
    
    <th>ClassID</th>
    
    <th>Action</th>
    </tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr>
        
        <td>" . $row["section"]. "</td>
        <td>" . $row["student_ID"]. "</td>
        <td>" . $row["classID"]. "</td>
    
        <td>
        <a onclick=\"myFunction('http://localhost/school/classDelete.php?id=".$row["classID"]."')\"><span class='glyphicon glyphicon-trash'></span></a>      
        <a href='http://localhost/school/classEdit.php?id=".$row["classID"]."'><span class='glyphicon glyphicon-pencil'></span></a></td>
        </tr>";

        echo "<script type=\"text/javascript\">
        function myFunction(newLocation) {
            if (confirm(\"Are you sure to Delete?\")) {
                console.log(newLocation);
                window.location = newLocation;
            } else {

            }
        }
        </script>";
    }
    echo "</table>";
} 
else {
    echo "0 results";
}

$conn->close();
?> 